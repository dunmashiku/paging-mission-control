const _ = require('lodash');

module.exports = (telemetryData) => {
    const satellites = telemetryData.split('\n');
    let satelliteCollection = [];
    let satelliteIds = [];
    let satelliteTimestampIntervalStart = '';
    let satelliteTimestampIntervalEnd = '';

    _.forEach(satellites, val => {
        const row = val.split('|');
        let satellite = {};
        [   
            satellite.timestamp, 
            satellite.satelliteId, 
            satellite.redHighLimit, 
            satellite.yellowHighLimit, 
            satellite.yellowLowLimit,
            satellite.redLowLimit, 
            satellite.rawValue, 
            satellite.component 
        ] = row;
        satelliteCollection.push(satellite);
    });

    return satelliteCollection;
}