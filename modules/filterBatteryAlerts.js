const _ = require('lodash');
const formatDate = require('./formatDate');

module.exports = satelliteCollection => {
    const batteryAlerts = satelliteCollection.filter(sat => sat.component === 'BATT' && parseFloat(sat.rawValue) < parseFloat(sat.redLowLimit));

    // By getting the satelliteIds from this step, we are able to find easily the 3 occurences of a battery allert
    // Then we just check if we have 3 for each satellite present
    const satelliteIds = [...new Set(batteryAlerts.map(item => item.satelliteId))];
    let battResults =  [];

    _.forEach(satelliteIds, val => {
        const allerts = _.filter(batteryAlerts, { 'satelliteId': val });
        if (allerts.length >= 3) {
            let alertInfo = {}
            alertInfo.satelliteId = allerts[0].satelliteId;
            alertInfo.severity = 'RED LOW';     //An enum or central string module would be great here. Just left a string for brevity
            alertInfo.component = allerts[0].component;
            alertInfo.timestamp = formatDate(allerts[0].timestamp) ?? allerts[0].timestamp;
            battResults.push(alertInfo);
        }
    });

    return battResults;
}