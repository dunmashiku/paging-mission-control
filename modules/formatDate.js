module.exports = timestamp => {
    const [date, time] = timestamp.split(' ');
    const dt = date.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
    const [year, month, day] = dt.split('-');
    const [hours, minutes, seconds] = time.split(':')
    const [secs, millisecs] = seconds.split('.');

    // Keeping in mind that the Month is zero indexed in JavaScript Date
    let dttime = new Date(Date.UTC(year, month - 1, day, hours, minutes, secs, millisecs));
    return dttime;
}