const fs = require('fs');

module.exports =  (telemetryDataPath) => {
    const telemetryData = fs.readFileSync(telemetryDataPath, 'ascii');
    return telemetryData;
}