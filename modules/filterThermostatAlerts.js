const _ = require('lodash');
const formatDate = require('./formatDate');

module.exports = satelliteCollection => {
    const thermostatAlerts = satelliteCollection.filter(sat => sat.component === 'TSTAT' && parseFloat(sat.rawValue) > parseFloat(sat.redHighLimit));

    // By getting the satelliteIds from this step, we are able to find easily the 3 occurences of a battery allert
    // Then we just check if we have 3 for each satellite present
    const satelliteIds = [...new Set(thermostatAlerts.map(item => item.satelliteId))];
    let tstatResults =  [];

    _.forEach(satelliteIds, val => {
        const allerts = _.filter(thermostatAlerts, { 'satelliteId': val });
        if (allerts.length >= 3) {
            let alertInfo = {}
            alertInfo.satelliteId = allerts[0].satelliteId;
            alertInfo.severity = 'RED HIGH';    //An enum or central string module would be great here. Just left a string for brevity
            alertInfo.component = allerts[0].component;
            alertInfo.timestamp  = formatDate(allerts[0].timestamp) ?? allerts[0].timestamp;
            tstatResults.push(alertInfo);
        }
    });

    return tstatResults;
}