const _ = require('lodash');
const http = require('http');
const readFile = require('./modules/readFile');
const createCollection = require('./modules/createCollection');
const filterBatteryAlerts = require('./modules/filterBatteryAlerts');
const filterThermostatAlerts = require('./modules/filterThermostatAlerts');
const displayAlertResults = require('./modules/displayAlertResults');


// Read data from file and create data structure
const telemetryDataPath = './data/sat_status_telemetry.txt';
const telemetryData = readFile(telemetryDataPath);
const satelliteCollection = createCollection(telemetryData);

// Find thermostat allert data
const tstatAlerts = filterThermostatAlerts(satelliteCollection);
// Find battery allert data
const battAlerts = filterBatteryAlerts(satelliteCollection);

// Display results to console
displayAlertResults(tstatAlerts, battAlerts);

// Server
const server = http.createServer((req, res) => {
    res.end('Paging Mission Control Server!');
});

server.listen(3000, '127.0.0.1', () => {
    console.log('Listening to requests on port 3000');
})